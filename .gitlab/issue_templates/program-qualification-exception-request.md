<!-- Please complete this form to request an exception to GitLab for Open Source Program requirements for a specific account. -->

### Account status
<!-- Sales representative completes this section. -->

* Name of sales representative/contact: 
* Is this a paid customer? `{yes/no}`
* Salesforce link, if any: 
* Existing GitLab account link, if any: 

### Account details
<!-- Sales representative completes this section. -->

Product:

* Product customer is requesting: 
* Number of seats customer is requesting: 

Account holder contact information:

* Name: 
* Street address: 
* Email address: 

### Justification for request
<!-- Sales representative completes this section. -->

Why are you requesting this exception?  
*(for example, will this help retain them as customers, or is a potential paid deal attached?)*

> 

Please explain any additional details relevant to this account, such as previous engagements or agreements:

> 

### Requirements and exceptions detail
<!-- Sales representative completes this section. -->

Use the table below to explain GitLab for Open Source Program eligibility criteria the account **does** and **does not** meet. For more detail, see the [GitLab for Open Source Program qualification requirements](https://about.gitlab.com/handbook/marketing/community-relations/community-programs/opensource-program/#who-qualifies-for-the-gitlab-for-open-source-program).

| Requirement | Qualifies? | Supporting Evidence | Additional Notes (optional) |
| ----------- | ---------- | ------------------- | --------------------------- |
| Project carries OSI-approved license? | `Yes/No` | [e.g., link to license] | [optional] |
| Organization is not seeking profit? | `Yes/No` | [e.g., link to website] | [optional] |
| Project is publicly visible? | `Yes/No` | [Add link to publicly visible repos] | [optional] |

### Submission checklist
<!-- Sales representative completes this section. -->

* [ ] Name this issue `Exception request: customer-name`

### Processing and approvals
<!-- Sales representative completes this section. -->

**Instructions for submitter:** Please provide names and GitLab handles for relevant account executives and sales managers.

**Instructions for sales team:** Please check the box next to your name to acknowledge you have reviewed this exception request and approve it. 

| Role | Name | GitLab Handle |
| ---- | ---- | ------------- |
| Account Executive | `name` | `@gitlabhandle` |
| Sales Manager | `name` | `@gitlabhandle` |
| Open Source Program Manager | Bryan Behrenshausen | `@bbehr` |
| TAM (if relevant) | `name` | `@gitlabhandle` |

### Reference links
<!-- Open source program team completes this part. -->

* Opportunity: 
* Zendesk ticket: 

/label ~"Program Requirements Exception"  
/assign @akarsten1  
/confidential
